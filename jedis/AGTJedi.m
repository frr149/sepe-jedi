//
//  AGTJedi.m
//  jedis
//
//  Created by Fernando Rodríguez Romero on 12/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTJedi.h"

@implementation AGTJedi

-(id) initWithName:(NSString*) name
     midichlorians:(NSUInteger) midichlorians
        lightSaber:(AGTLightSaber*)lightSaber
         padawanOf:(AGTJedi*) master{
    
    if (self = [super initWithName:name]) {
        _midichlorians = midichlorians;
        _padawanOf = master;
        _lighSaber = lightSaber;
    }
    return self;
}

-(id) initWithName:(NSString*) name{
    
    return [self initWithName:name
                midichlorians:100
                   lightSaber:[AGTLightSaber blueLightSaber]
                    padawanOf:nil];
}

-(id) initWithJediMasterNamed:(NSString*) name{
    return [self initWithName:name
                midichlorians:1000
                   lightSaber:[AGTLightSaber greenLightSaber]
                    padawanOf:nil];
}

-(NSString*)unsheathe{
    return @"█||||||(•)█Ξ█████████████████████";
}

#pragma mark - NSObject
-(NSString *) description{
    
    return [NSString stringWithFormat:@"<%@: %@ (%lu) - %@>", [self class], [self name], (unsigned long)[self midichlorians], [self padawanOf]];
}

-(BOOL)isEqual:(id)object{
    
    if (self == object) {
        return YES;
    }
    
    if ([object isKindOfClass:[self class]]) {
        
        // comparo propiedades?
        return [[self proxyForComparison] isEqual: [object proxyForComparison]];
    }else{
        return NO;
    }
}

-(NSString *) proxyForComparison{
    return [NSString stringWithFormat:@"%@ %@ %lu %@", [self name], [self padawanOf], [self midichlorians],[self lighSaber] ];
}

@end










