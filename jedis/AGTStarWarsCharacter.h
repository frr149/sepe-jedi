//
//  AGTStarWarsCharacter.h
//  jedis
//
//  Created by Fernando Rodríguez Romero on 12/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGTStarWarsCharacter : NSObject

@property NSString *name;

+(instancetype) starWarsCharacterWithName:(NSString*) name;

-(id) initWithName:(NSString *) name;

@end
