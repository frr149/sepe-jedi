//
//  AGTLightSaber.h
//  jedis
//
//  Created by Fernando Rodríguez Romero on 12/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface AGTLightSaber : NSObject
@property NSColor* color;

// class methods
+(instancetype) lightSaberWithRed:(float)red
                            green:(float)green
                             blue:(float) blue;

+(instancetype) blueLightSaber;
+(instancetype) redLightSaber;
+(instancetype) greenLightSaber;
+(instancetype) purpleLightSaber;

//designated
-(id) initWithRed:(float)red
            green:(float)green
             blue:(float) blue;

-(id) initWithBlueLightSaber;
-(id) initWithRedLightSaber;
-(id) initWithGreenLightSaber;
-(id) initWithPurpleLightSaber;




@end
