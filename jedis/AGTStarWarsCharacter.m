//
//  AGTStarWarsCharacter.m
//  jedis
//
//  Created by Fernando Rodríguez Romero on 12/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTStarWarsCharacter.h"

@implementation AGTStarWarsCharacter

+(instancetype) starWarsCharacterWithName:(NSString*) name{
    return [[self alloc] initWithName:name];
}

-(id) initWithName:(NSString *) name{
    
    if (self = [super init]) {
        _name = name;
    }
    return self;
}

-(NSString*) description{
    return [NSString stringWithFormat:@"<%@:%@>", [self class], [self name]];
}

-(BOOL) isEqual:(id)object{
    
    if (self == object) {
        return YES;
    }
    
    if ([object isKindOfClass:[self class]]) {
        return [self.name isEqual:[object name]];
    }else{
        return NO;
    }
}










@end
