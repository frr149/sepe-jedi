//
//  AGTJedi.h
//  jedis
//
//  Created by Fernando Rodríguez Romero on 12/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTStarWarsCharacter.h"
#import "AGTLightSaber.h"

@interface AGTJedi : AGTStarWarsCharacter

@property NSUInteger midichlorians;
@property AGTLightSaber *lighSaber;
@property AGTJedi *padawanOf;

// designated
-(id) initWithName:(NSString*) name
     midichlorians:(NSUInteger) midichlorians
        lightSaber:(AGTLightSaber*)lightSaber
         padawanOf:(AGTJedi*) master;

-(id) initWithName:(NSString*) name;

-(id) initWithJediMasterNamed:(NSString*) name;

-(NSString*)unsheathe;

@end











